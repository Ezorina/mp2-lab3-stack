#ifndef __CALCULATION_H__
#define __CALCULATION_H__

#include <string>
#include <iostream>
#include "tstack.h"
#include "tsimplestack.h"
using namespace std;

int BracketsControl(const string str)	//�������� ����������� ������
{
	TStack st(20);
	int size = str.length();
	int brNum = 0;
	int Errors = 0;

	cout << "Brackets control:" << endl;
	cout << "(" << "	" << ")" << endl;

	for (int i = 0; i < size; i++)
	{
		if (str[i] == '(')
			st.Put(++brNum);
		if (str[i] == ')')
		{
			brNum++;
			if (st.IsEmpty())
			{
				Errors++;
				cout << '0' << "	" << brNum << endl;
			}
			else
				cout << st.Get() << "	" << brNum << endl;
		}
	}

	while (!st.IsEmpty())
	{
		cout << st.Get() << "       " << '0' << endl;
		Errors++;
	}
	cout << "Number of errors: " << Errors << endl;

	return Errors;
}

int PriorityOf(const char ch)	//���������� ��������� ��������
{
	switch (ch)
	{
		case '(': return 0; break;
		case ')': return 1; break;
		case '+': return 2; break;
		case '-': return 2; break;
		case '*': return 3; break;
		case '/': return 3; break;
		default: return -1;
	}
}

int OperNum(const char ch)		//���������� ����� ��������
{
	switch (ch)
	{
		case '(': return 10; break;
		case ')': return 11; break;
		case '+': return 1; break;
		case '-': return 2; break;
		case '*': return 3; break;
		case '/': return 4; break;
		default: return -1;
	}
}

char OperSym(const int num)		//���������� ������ ��������
{
	switch (num)
	{
		case 10: return '('; break;
		case 11: return ')'; break;
		case 1: return '+'; break;
		case 2: return '-'; break;
		case 3: return '*'; break;
		case 4: return '/'; break;
		default: return -1;
	}
}

string PostfixForm(const string &instr) //�������������� � ����������� �����
{									//(��������������� ������������ ���������)
	string poststr;
	TStack st;
	int pos = 0;
	char ch;

	while (pos < instr.size())
		{
			ch = instr[pos++];
			if (('0' <= ch) && (ch <= '9') || (ch == '.'))	//���� ������ - �������
				poststr += ch;
			if (ch == '+' || ch == '-' || ch == '*' || ch == '/')			//���� ������ - ��������
			{
				if (st.IsEmpty())
					st.Put(OperNum(ch));
				else if (PriorityOf(ch)>PriorityOf(OperSym(st.Top())))
					st.Put(OperNum(ch));
				else
				{
					while (true)
					{
						poststr += OperSym(st.Get());
						if (PriorityOf(ch) > PriorityOf(OperSym(st.Top())) || st.IsEmpty())
							break;
					}
					st.Put(OperNum(ch));
				}
				poststr += ' ';
			}
			if (ch == '(')		//���� ������ - ����������� ������
				st.Put(OperNum(ch));

			if (ch == ')') // ���� ������ - ����������� ������
			{			   
				while (true)
				{
					if (st.IsEmpty())
						break;
					if (st.Top() == OperNum('('))
					{
						st.Get();
						break;
					}
					poststr += OperSym(st.Get());
				}
			}
		}

		while (!st.IsEmpty()) 
			poststr += OperSym(st.Get());
		return poststr;
}



double Calculation(const string &str) //���������� ��������� � ����������� ����
{
	
	TStack st;
	int pos = 0, i = 0, j = 0;
	char number[10] = "";
	double temp, fract;
	string postfix = PostfixForm(str);
	while (pos < postfix.size())
	{
		if (('0' <= postfix[pos]) && (postfix[pos] <= '9') || (postfix[pos] == '.')) 
			number[i++] = postfix[pos];
		else 
		{
			number[i] = '\0';
			temp = 0.0; fract = 1.0;
			for (j = 0; (number[j] != '\0') && (number[j] != '.'); j++)
				temp = temp * 10.0 + (double)(number[j] - '0');
			if (number[j] == '.')
			{
				for (j = j + 1; number[j] != '\0'; j++)
				{
					fract *= 0.1;
					temp = temp + (double)(number[j] - '0')*fract;
				}
			}
			if (j != 0)
				st.Put(temp); 
			number[0] = '\0'; 
			i = 0; 
			j = 0;
			if (postfix[pos] == ' ') 
			{
				pos++;
				continue;
			}
			temp = st.Get();
			switch (postfix[pos])
			{
				case '+': st.Put(st.Get() + temp); break;
				case '-': st.Put(st.Get() - temp); break;
				case '*': st.Put(st.Get() * temp); break;
				case '/': st.Put(st.Get() / temp); break;
			}
		}
		pos++;
	}
	return st.Get();
}


double Calculate(string &str)
{
	if (BracketsControl(str) == 0)
	{
		return Calculation(PostfixForm(str));
	}
	return 0;
}

#endif