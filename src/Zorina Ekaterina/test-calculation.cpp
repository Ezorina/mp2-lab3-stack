#include "calculation.h"

#include <gtest.h>


TEST(Calculation, can_check_right_brackets_placement)
{
	EXPECT_EQ(0, BracketsControl("(1+2)/(3+4*6.7)-5.3*4.4"));
}

TEST(Calculation, can_detect_incorrect_brackets_placement)
{
	EXPECT_NE(0, BracketsControl("(3+1)/2+6.5)*(4.8+("));
}

TEST(Calculation, can_convert_an_expression_with_int_nums)
{
	EXPECT_EQ(PostfixForm("3*(10-5)+7"), "3 10 5-* 7+");
}

TEST(Calculation, can_convert_an_expression_with_real_nums)
{
	EXPECT_EQ(PostfixForm("(1+2)/(3+4*6.7)-5.3*4.4"), "1 2+ 3 4 6.7*+/ 5.3 4.4*-");
}

TEST(Calculation, can_calculate_an_expression)
{
	double res = (1 + 2) / (3 + 4 * 6.7) - 5.3*4.4;
	EXPECT_DOUBLE_EQ(res, Calculation("(1+2)/(3+4*6.7)-5.3*4.4"));
}

TEST(Calculation, not_calculate_an_exp_with_incorrect_placement_of_pars)
{
	ASSERT_ANY_THROW(Calculation("(1+2)(/())3+4*6.7)-5.3*4.4)("));
}
